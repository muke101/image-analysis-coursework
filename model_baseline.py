import numpy as np
import glob
import matplotlib.pyplot as plt
import cv2
import matplotlib.image as mpimg
import tensorflow.compat.v1 as tf
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, AveragePooling2D
from keras.layers import Conv2D, MaxPooling2D, BatchNormalization
from keras.applications import ResNet50
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import shuffle
from random import randint
import os
import csv

tf.GPUOptions(allow_growth=True)

images = []
binarised = []
classes = []
x_test = []
num_classes = 10

def binarise(org, threshholds):
    bsd = np.zeros((org.shape[0],org.shape[1]), dtype='int')
    bsd[org[:,:,0] > threshholds[0]] = 255
    bsd[org[:,:,1] < threshholds[1]] = 255
    bsd[org[:,:,2] > threshholds[2]] = 255
    return bsd

def augment_background(im,i):
    bsd = binarised[i] 
    #background = (randint(1,254), randint(1,254), randint(1,254))
    background = randint(1,254)
    im[np.where(bsd == 0)] = background
    return im

def preprocess(im):
    im = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
    im = clahe.apply(im)
    im = np.reshape(im, (im.shape[0],im.shape[1],1))
    return im

threshholds = [[50,50,100],[60,85,120],[70,85,145],[60,85,85],[60,90,85],[70,90,120],[70,90,85]]

for i in range(1,8):
    filenames = glob.glob("items-"+str(i)+"/resized*")
    classifications = {name:cls for name,cls in csv.reader(open("items-"+str(i)+"/classifications"))}
    discarded = [name.strip() for name in open("items-"+str(i)+"/discarded","r")]
    for path in filenames:
        name = path.split("/")[1].split('.')[0][8:] #i should really just learn re's
        if name not in discarded:
            image = cv2.imread(path)
            image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
            images.append(image)
            binarised.append(binarise(image, threshholds[i-1]))
            cls = classifications[name] 
            classes.append(cls)


test_data = glob.glob("video_3/resized*")
for path in test_data:
    discarded = [name.strip() for name in open("video_3/discarded","r")]
    name = path.split("/")[1].split('.')[0][8:]
    if name not in discarded:
        im = cv2.imread(path)
        im = cv2.cvtColor(im, cv2.COLOR_BGR2RGB)
        x_test.append(im)

x_test = np.array(x_test).astype('float')/255.0

images, classes = shuffle(images, classes, random_state=0)

n70 = int(0.7 * len(images))
x_train, x_val = images[:n70], images[n70:]
y_train, y_val = classes[:n70], classes[n70:]

augmented = []
#for i in range(len(x_train)):
#    im = cv2.GaussianBlur(x_train[i], (9,9), 0)
#    im = np.reshape(im, (im.shape[0],im.shape[1],1))
#    augmented.append((im,y_train[i]))
#    im = augment_background(x_train[i],i)
#    augmented.append((im,y_train[i]))
#
#for i in augmented:
#    x_train.append(i[0])
#    y_train.append(i[1])

x_train, y_train = shuffle(x_train, y_train, random_state=0)

x_train = np.array(x_train).astype('float')/255.0
y_train = np.array(y_train)
x_val = np.array(x_val).astype('float')/255.0
y_val = np.array(y_val)

input_shape = images[0].shape
y_train = keras.utils.to_categorical(y_train)
y_val = keras.utils.to_categorical(y_val)

datagen_train = ImageDataGenerator(
        featurewise_center=True,
        featurewise_std_normalization=True)
        #brightness_range=[20,20],
        #shear_range=20,
        #zoom_range=20,
        #vertical_flip=True,
        #rotation_range=20,
        #width_shift_range=0.2,
        #height_shift_range=0.2,
        #horizontal_flip=True)
datagen_train.fit(x_train)

datagen_test = ImageDataGenerator(
        featurewise_center=True,
        featurewise_std_normalization=True)
datagen_test.fit(x_train)

model = Sequential()

model.add(Conv2D(8, (3,3), activation='relu', strides=2,input_shape=input_shape, padding='same'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))

model.add(Conv2D(16, (3,3), activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))

model.add(Conv2D(32, (3,3), activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3,3), activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))

model.add(Conv2D(64, (3,3), activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))

model.add(Conv2D(128, (3,3), activation='relu', padding='same'))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.25))

model.add(Flatten())

model.add(Dense(num_classes, activation='softmax'))

model.summary()

batch_size = 512
epochs = 80

model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adam(learning_rate=0.0001), metrics=['accuracy'])

history = model.fit(x_train, y_train, batch_size=batch_size, epochs=epochs, verbose=2, validation_data=(x_val,y_val))

y_test = np.argmax(model.predict(x_test), axis=-1)

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

for i in range(len(x_test)):
    print("Predicted=%s" % y_test[i])
    plt.imshow(x_test[i])
    plt.show()

