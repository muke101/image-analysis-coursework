from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import cv2
import glob

images = []
names = []

def populate(filetype):
    for path in glob.glob("items-*/*."+filetype):
        p = path.split('/')
        item = p[0]
        name = p[1]
        discarded = [i.strip() for i in open(item+"/discarded","r").readlines()]
        if name.split('.')[0] not in discarded:
            names.append(p)
            images.append(np.asarray(Image.open(path)))
    
    for i, image in enumerate(images):
        image = cv2.resize(image, (224, 224), interpolation = cv2.INTER_AREA)
        path = names[i][0]
        name = names[i][1]
        image = Image.fromarray(image)
        image.save(path+"/"+"resized_"+name)

populate("jpeg")
populate("png")
