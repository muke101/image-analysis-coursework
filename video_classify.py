import sys
import csv
import os
import glob

video = sys.argv[1]
start = sys.argv[2]

n = input("number of images: ")

discarded = open("video_"+video+"/discarded", "w")

def feh(path):
    os.system("feh "+path)

for path in glob.glob("video_"+video+"/*.jpeg"):
    feh(path)

    cls = input(": ")

    if cls == "1":
        discarded.write(path.split("/")[1]+"\n")

discarded.close()
