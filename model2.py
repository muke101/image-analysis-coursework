import numpy as np
import glob
import matplotlib.pyplot as plt
import cv2
import matplotlib.image as mpimg
import tensorflow.compat.v1 as tf
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten
from keras.layers import Conv2D, MaxPooling2D
from keras.applications import ResNet50
from keras.preprocessing.image import ImageDataGenerator
from sklearn.utils import shuffle
import os
import csv

tf.GPUOptions(allow_growth=True)

images = []
classes = []
num_classes = 10

for i in range(1,8):
    filenames = glob.glob("items-"+str(i)+"/resized*")
    classifications = {name:cls for name,cls in csv.reader(open("items-"+str(i)+"/classifications"))}
    discarded = [name.strip() for name in open("items-"+str(i)+"/discarded")]
    for path in filenames:
        name = path.split("/")[1].split('.')[0][8:] #i should really just learn re's
        if name not in discarded:
            image = cv2.imread(path)
            image = cv2.cvtColor(image,cv2.COLOR_BGR2RGB)
            images.append(image)
            cls = classifications[name] 
            classes.append(cls)

x = np.array(images).astype('float')/255.0
y = np.array(classes)

x, y = shuffle(x, y, random_state=0)

n70 = int(0.7 * x.shape[0])

x_train, x_val = x[:n70], x[n70:]
y_train, y_val = y[:n70], y[n70:]

#we are channels_last
img_rows, img_cols, num_channels = images[0].shape
x_train = x_train.reshape(x_train.shape[0], img_rows, img_cols, num_channels)
x_val = x_val.reshape(x_val.shape[0], img_rows, img_cols, num_channels)
input_shape = (img_rows, img_cols, num_channels)
y_train = keras.utils.to_categorical(y_train)
y_val = keras.utils.to_categorical(y_val)

datagen = ImageDataGenerator(
        featurewise_center=True,
        featurewise_std_normalization=True,
        rotation_range=20,
        width_shift_range=0.2,
        height_shift_range=0.2,
        horizontal_flip=True)

datagen.fit(x_train)

model = Sequential()

model.add(Conv2D(8, (5,5), activation='relu', input_shape=input_shape))
model.add(MaxPooling2D(pool_size=(2,2)))
model.add(Dropout(0.4))

model.add(Conv2D(16, (5,5), activation='relu'))
model.add(MaxPooling2D(pool_size=(4,4)))
model.add(Dropout(0.4))

model.add(Flatten())

#model.add(Dense(, activation='relu'))
#model.add(Dropout(0.40))

model.add(Dense(num_classes, activation='softmax'))

model.summary()

batch_size = 64
epochs = 100

model.compile(loss=keras.losses.categorical_crossentropy, optimizer=keras.optimizers.Adam(learning_rate=0.001), metrics=['accuracy'])

history = model.fit(datagen.flow(x_train, y_train, batch_size=batch_size), epochs=epochs, verbose=2, validation_data=(x_val, y_val))

plt.plot(history.history['accuracy'])
plt.plot(history.history['val_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()
