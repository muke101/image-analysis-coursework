from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import glob

def pad(im, i, ymax, xmax):
    new = np.zeros((ymax,xmax,3), dtype='uint8')
    if im.shape[2] == 4:
        im = im[:,:,:3]
    new[0:im.shape[0],0:im.shape[1]] = im
    image = Image.fromarray(new)
    path = names[i][0] 
    name = names[i][1]
    image.save(path+"/"+"padded_"+name)

images = []
names = []

def populate(filetype, ymax, xmax):
    for path in glob.glob("items-*/*."+filetype):
        p = path.split('/')
        item = p[0]
        name = p[1]
        discarded = [i.strip() for i in open(item+"/discarded","r").readlines()]
        if name.split('.')[0] not in discarded:
            names.append(p)
            images.append(np.asarray(Image.open(path)))

    for image in images:
        ymax = max(ymax,image.shape[0]) 
        xmax = max(xmax,image.shape[1])
    
    for i, image in enumerate(images):
        pad(image, i,ymax,xmax)

    return (ymax,xmax)

ymax, xmax = populate("jpeg",0,0)
populate("png",ymax,xmax) #hack, please ignore!
