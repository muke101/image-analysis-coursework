import sys
import csv
import os

image = sys.argv[1]
path = sys.argv[2]
start = sys.argv[3]

n = input("number of images: ")

f = open("train-"+image+".csv", "w")
csv = csv.writer(f)
csv.writerow(["part_num","start_x","start_y","end_x","end_y"])
discarded = open(path+"discarded", "w")
classifications = open(path+"classifications", "w")

def feh(path, i):
    os.system("feh "+path+str(i)+".jpeg")

def classify():
    cls = input('classification:' )
    return cls


for i in range(int(start),int(n)+1):
    coords = open(path+str(i),"r").read().split(",")
    
    xmin = coords[0]
    ymin = coords[1]
    xmax = coords[2]
    ymax = coords[3]

    feh(path,i)
    cls = classify()
    print(i)

    if cls == -1:
        discarded.write(str(i)+"\n")
        continue
    csv.writerow([cls, xmin, ymin, xmax, ymax])
    classifications.write(str(i)+","+str(cls)+"\n")

f.close()
discarded.close()
classifications.close()

