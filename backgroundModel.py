from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
from skimage import measure
import scipy
from multiprocessing import Pool
import csv
import cv2

def binarise(org):
    bsd = np.zeros((org.shape[0],org.shape[1]), dtype='int')
    bsd[org[:,:,0] > threshholds[0]] = 1
    bsd[org[:,:,1] < threshholds[1]] = 1
    bsd[org[:,:,2] > threshholds[2]] = 1
    return bsd

def outputBinarised(frames):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    writer = cv2.VideoWriter('binairsed.mp4', fourcc, 24, (frames[0].shape[1],frames[0].shape[0]), True)

    for frame in frames:
        frame = binarise(frame)
        frame[np.where(frame == 1)] = 255
        frame = frame.astype(np.uint8)
        frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
        writer.write(frame)
    writer.release()

def box(comps, label):
    xx, yy = np.meshgrid(np.arange(0,comps.shape[1]), np.arange(0,comps.shape[0]))
    where_x = xx[comps==label] 
    where_y = yy[comps==label] 
    xmin = np.min(where_x)
    xmax = np.max(where_x)
    ymin = np.min(where_y)
    ymax = np.max(where_y)
    return xmin, ymin, xmax, ymax

def save_image(box,i,j,k):
    image = Image.fromarray(box)
    image.save("video_"+str(k)+"/"+str(i)+"_"+str(j)+".jpeg")

def save_coords(xmin,ymin,xmax,ymax,i):
    coord = open("video-1.csv","w")
    coord.writelines(str(xmin)+","+str(ymin)+","+str(xmax)+","+str(ymax))

def outputMask(masks):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    writer = cv2.VideoWriter('masked.mp4', fourcc, 30, (masks[0].shape[1], masks[0].shape[0]), True)

    for mask in masks:
        mask = mask.astype(np.uint8)
        mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        writer.write(mask)
    writer.release()

def getFrames(path):
    vid = cv2.VideoCapture(path)

    frames = []

    while True:
        success, frame = vid.read()

        if success:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frames.append(frame)

        else:
            break

    vid.release()

    return frames

frames = getFrames('test-2.m4v')
#for i in range(50):
#    frames[0:0] = frames[0:10]
#frames[10:10] = [frames[10] for i in range(100)]

masks = []

fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=False)

for frame in frames:
    #fig = plt.figure()
    #ax = fig.add_subplot(111)
    masks.append(fgbg.apply(frame))
    #comps = measure.label(mask, background = 0)
    #ax.imshow(frame)
    #for l in np.unique(comps)[1:]:
    #    min_size = 5000
    #    xmin, ymin, xmax, ymax = box(comps,l)
    #    bb = frame[ymin:ymax,xmin:xmax,:]
    #    n = np.count_nonzero(bounding_box)
    #    if n > min_size:
    #        rect = patches.Rectangle((bb[0], bb[1]), bb[2]-bb[0],bb[3]-bb[1],edgecolor='r')
    #        ax.add_patch(rect)
    #plt.show()

outputMask(masks)
