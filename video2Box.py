from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
from skimage import measure
import scipy
from multiprocessing import Pool
import csv
import cv2

def histograms(im):
    org = np.asarray(im)
    org_r = org[:,:,0]
    org_g = org[:,:,1]
    org_b = org[:,:,2]
    plt.hist(org_r.ravel(), bins=30, range=[0,256])
    plt.show()
    plt.hist(org_g.ravel(), bins=30, range=[0,256])
    plt.show()
    plt.hist(org_b.ravel(), bins=30, range=[0,256])
    plt.show()

def colour_histograms(im):
    hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
    org = np.asarray(hsv)
    hue = org[:,:,0]
    sat = org[:,:,1]
    v = org[:,:,2]
    plt.hist(hue.ravel(), bins=30, range=[0,256])
    plt.show()
    plt.hist(sat.ravel(), bins=30, range=[0,256])
    plt.show()
    plt.hist(v.ravel(), bins=30, range=[0,256])
    plt.show()

def binarise(org):
    bsd = np.zeros((org.shape[0],org.shape[1]), dtype='int')
    bsd[org[:,:,0] > threshholds[0]] = 1
    bsd[org[:,:,1] < threshholds[1]] = 1
    bsd[org[:,:,2] > threshholds[2]] = 1
    return bsd

def colour_binarise(im, threshholds):
    hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
    org = np.asarray(hsv)
    bsd = np.zeros((org.shape[0],org.shape[1],org.shape[2]), dtype='uint8')
    bsd[org[:,:,0] > threshholds[0]] = 255
    bsd[org[:,:,1] > threshholds[1]] = 255
    bsd[org[:,:,2] < threshholds[2]] = 255
    bsd = cv2.cvtColor(bsd, cv2.COLOR_HSV2RGB)
    bsd = cv2.cvtColor(bsd, cv2.COLOR_RGB2GRAY)
    return bsd

def outputMasks(masks):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    writer = cv2.VideoWriter('masked.mp4', fourcc, 30, (masks[0].shape[1], masks[0].shape[0]), True)

    for mask in masks:
        mask = mask.astype(np.uint8)
        mask = cv2.cvtColor(mask, cv2.COLOR_GRAY2BGR)
        writer.write(mask)
    writer.release()

def output_colour_binarised(frames,threshholds):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    writer = cv2.VideoWriter('colour_binairsed.mp4', fourcc, 24, (frames[0].shape[1],frames[0].shape[0]), True)

    for frame in frames:
        frame = colour_binarise(frame,threshholds)
        frame[np.where(frame != 0)] = 255
        frame = frame.astype(np.uint8)
        frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
        writer.write(frame)
    writer.release()


def outputBinarised(frames):
    fourcc = cv2.VideoWriter_fourcc(*'mp4v')
    writer = cv2.VideoWriter('binairsed.mp4', fourcc, 24, (frames[0].shape[1],frames[0].shape[0]), True)

    for frame in frames:
        frame = binarise(frame)
        frame[np.where(frame == 1)] = 255
        frame = frame.astype(np.uint8)
        frame = cv2.cvtColor(frame, cv2.COLOR_GRAY2BGR)
        writer.write(frame)
    writer.release()

def box(comps, label):
    xx, yy = np.meshgrid(np.arange(0,comps.shape[1]), np.arange(0,comps.shape[0]))
    where_x = xx[comps==label] 
    where_y = yy[comps==label] 
    xmin = np.min(where_x)
    xmax = np.max(where_x)
    ymin = np.min(where_y)
    ymax = np.max(where_y)
    return xmin, ymin, xmax, ymax

def save_image(box,i,j,k):
    image = Image.fromarray(box)
    image.save("video_"+str(k)+"/"+str(i)+"_"+str(j)+".jpeg")

def save_coords(n,xmin,ymin,xmax,ymax,i):
    coord = open("video_"+str(i)+"/test-"+str(i)+".csv","a")
    coord.writelines(str(n)+","+str(xmin)+","+str(ymin)+","+str(xmax)+","+str(ymax)+"\n")

def findBoxes(data):
    im = data[0]
    i = data[1]
    k = data[2]
    threshhold = data[3]
    org = np.asarray(im)
    bsd = colour_binarise(org,threshhold)
    comps = measure.label(bsd, background=0)
    uniques = np.unique(comps)[1:]
    min_size = 100000
    j = 0
    for l in uniques:
        xmin, ymin, xmax, ymax = box(comps,l)
        bounding_box = org[ymin:ymax,xmin:xmax,:]
        n = np.count_nonzero(bounding_box)
        if n > min_size:
            j += 1
            save_image(bounding_box,i,j,k)
            save_coords(i,xmin,ymin,xmax,ymax,k)

def getFrames(path):
    vid = cv2.VideoCapture(path)

    frames = []

    while True:
        success, frame = vid.read()

        if success:
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frames.append(frame)

        else:
            break
    vid.release()

    return frames

def main(filename, sample, k):
    p = Pool(10) 
    frames = getFrames(filename)

    output_colour_binarised(frames,threshholds)
   # p.map(findBoxes, [(frames[i], i, k, threshholds) for i in range(len(frames)) if i % sample == 0])

frames = getFrames("test-2.m4v")

colour_histograms(frames[0])
#
threshholds = [170,70,150]
#
#output_colour_binarised(frames)

#frames = getFrames("test-3.m4v")
#
#fgbg = cv2.createBackgroundSubtractorMOG2(detectShadows=False, varThreshold=50, history=120)
#
#masks = []
#
#for frame in frames:
#    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
#    masks.append(fgbg.apply(frame))
#
#outputMasks(masks)


##threshholds = [75,110,110]
#threshholds = [60,67]
#
#main('test-1.m4v', 10, 1)
#main('test-2.m4v', 10, 2)
#main('test-3.m4v', 10, 3)
