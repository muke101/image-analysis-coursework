from PIL import Image
import numpy as np
import matplotlib.pyplot as plt
import pickle
import os
from skimage import measure
import scipy
from multiprocessing import Pool
import csv


def histograms(org):
    org_r = org[:,:,0]
    org_g = org[:,:,1]
    org_b = org[:,:,2]
    plt.hist(org_r.ravel(), bins=30, range=[0,256])
    plt.show()
    plt.hist(org_g.ravel(), bins=30, range=[0,256])
    plt.show()
    plt.hist(org_b.ravel(), bins=30, range=[0,256])
    plt.show()

def binarise(org, threshholds):
    bsd = np.zeros((org.shape[0],org.shape[1]), dtype='int')
    bsd[org[:,:,0] > threshholds[0]] = 1
    bsd[org[:,:,1] < threshholds[1]] = 1
    bsd[org[:,:,2] > threshholds[2]] = 1
    return bsd

def box(comps, label):
    xx, yy = np.meshgrid(np.arange(0,comps.shape[1]), np.arange(0,comps.shape[0]))
    where_x = xx[comps==label] 
    where_y = yy[comps==label] 
    xmin = np.min(where_x)
    xmax = np.max(where_x)
    ymin = np.min(where_y)
    ymax = np.max(where_y)
    return xmin, ymin, xmax, ymax

def save_image(box,i,j):
    image = Image.fromarray(box)
    image.save("items-"+str(i)+"/"+str(j)+".jpeg")

def save_coords(xmin,ymin,xmax,ymax,i,j):
    coord = open("items-"+str(i)+"/"+str(j),"w")
    coord.writelines(str(xmin)+","+str(ymin)+","+str(xmax)+","+str(ymax))

def findBoxes(data):
    im = data[0]
    i = data[1]
    threshholds = data[2]
    org = np.asarray(im)
    #histograms(org)
    bsd = binarise(org, threshholds)
    comps = measure.label(bsd, background=0)
    uniques = np.unique(comps)[1:]
    min_size = 5000
    j = 0
    for l in uniques:
        xmin, ymin, xmax, ymax = box(comps,l)
        bounding_box = org[ymin:ymax,xmin:xmax,:]
        n = np.count_nonzero(bounding_box)
        if n > min_size:
            j += 1
            #save_image(box,i,j)
            save_coords(xmin,ymin,xmax,ymax,i,j)

if os.path.isfile("./images.pickle"):
    images = pickle.load(open("images.pickle","rb"))
else:
    images = []
    
    for i in range(1,8):
        images.append(Image.open("./train-"+str(i)+".jpeg"))

    pickle.dump(images, open("images.pickle","wb"))

threshholds = [[50,50,100],[60,85,120],[70,85,145],[60,85,85],[60,90,85],[70,90,120],[70,90,85]]

p = Pool(len(images))
p.map(findBoxes, [(images[i], i+1, threshholds[i]) for i in range(len(images))])
